package be.foreach.bitbucket.hook;

/**
 * @author ND
 * @since 16/11/2014
 */
public abstract class NotificationSettings {
    // General
    public static final String FIELD_BRANCH_BLACKLIST = "branchBlacklist";
    public static final String FIELD_BRANCH_WHITELIST = "branchWhitelist";
    public static final String ENABLE_MAIL_NOTIFICATION = "mailNotification";
    public static final String ENABLE_SLACK_NOTIFICATION = "slackNotification";
    public static final String ERROR_NOTIFICATION_TYPES = "notificationTypesError";

    // Mail notifications
    public static final String FIELD_MAIL_TO = "mailToAddress";
    public static final String FIELD_MAIL_OVERRIDES_FROM = "mailOverrideFrom";
    public static final String FIELD_MAIL_OVERRIDES_SUBJECT = "mailOverrideSubject";
    public static final String FIELD_MAIL_OVERRIDES_BODY = "mailOverrideBody";
    public static final String TEMPLATE_FROM = "mailFromTemplate";
    public static final String TEMPLATE_SUBJECT = "mailSubjectTemplate";
    public static final String TEMPLATE_BODY = "mailBodyTemplate";

    // Slack notifications
    public static final String FIELD_SLACK_CHANNELS = "slackChannels";
    public static final String FIELD_SLACK_WEB_HOOK_URL = "slackWebHookUrl";
}
